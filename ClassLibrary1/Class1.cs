﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    
    public class Pair<T>: IComparable<Pair<T>> where T:IComparable<T>
    {
        T first, second;
        public Pair() { first = default(T); second = default(T); }
        public Pair(T f, T s) { first = f; second = s; }
        public int CompareTo(Pair<T> other)
        {
            return first.CompareTo(other.first);

        }
    }
    
}
